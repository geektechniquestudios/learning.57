package com.geektechnique.hyperrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HyperRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(HyperRestApplication.class, args);
    }

}
